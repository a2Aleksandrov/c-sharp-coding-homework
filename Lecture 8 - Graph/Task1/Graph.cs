﻿public class Graph
{
    private int _vertexCount;
    List<List<int>> matrix;
    public Graph(int count)
    {
        _vertexCount = count;
        matrix = new List<List<int>>();
        for (int i = 0; i < _vertexCount; i++)
        {
            matrix.Add(new List<int>());
            for (int j = 0; j < _vertexCount; j++)
            {
                matrix[i].Add(0);
            }
        }
    }
    public void AddEdge(int start, int end)
    {
        matrix[start][end] = 1;
        matrix[end][start] = 1;
    }
    public bool FindCycle(int start)
    {
        Stack<int> orderedNodes = new Stack<int>();
        HashSet<int> visited = new HashSet<int>();
        orderedNodes.Push(start);

        while (orderedNodes.Count > 0)
        {
            var current = orderedNodes.Pop();
            if (orderedNodes.Contains(current))
            {
                return true;
            }
            visited.Add(current);

            for (int i = 0; i < _vertexCount; i++)
            {
                if (matrix[current][i] == 1 && !visited.Contains(i))
                {
                    orderedNodes.Push(i);
                }
            }
        }
        return false;
    }
}
