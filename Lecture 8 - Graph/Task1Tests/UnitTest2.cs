﻿namespace Task1Tests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void returnsTrueWhenMultiplecycles()
        {
            var graph = new Graph(5);

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);
            graph.AddEdge(1, 2);
            graph.AddEdge(0, 4);
            graph.AddEdge(2, 3);
            bool actual = graph.FindCycle(2);
            Assert.IsTrue(actual);
        }
    }
}
