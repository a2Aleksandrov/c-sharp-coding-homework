namespace Task1Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void returnsFalseWithNullInput()
        {
            var graph = new Graph(0);
            bool actual = graph.FindCycle(0);
            Assert.IsFalse(actual);
        }
        
    }
}