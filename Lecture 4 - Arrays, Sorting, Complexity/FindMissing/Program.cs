﻿using System.Reflection.Metadata.Ecma335;

namespace findMissingNS
{
    public class FindMissingClass
    {
        private int[] initialArray;
        public FindMissingClass(int[] allNums)
        {
            initialArray = allNums;
        }

        public int FindMissing(int[] input)
        {
            int missingNum = -1;
            bool foundCurrent = false;
            for (int i = 1; i < initialArray.Length - 1; i++)
            {
                foundCurrent = false;
                for (int j = 0; j < input.Length; j++)
                {
                    if (initialArray[i] == input[j])
                    {
                        foundCurrent = true;
                        missingNum = -1;
                        break;
                    }
                    if (foundCurrent == false)
                    {
                        missingNum = initialArray[i];
                    }
                }
                if (foundCurrent == false)
                {
                    break;
                }
            }
            Console.WriteLine(missingNum);
            return missingNum;
        }
        public static void Main()
        {

            FindMissingClass result = new FindMissingClass(new int[] { 1,2,5,3,4 });
            result.FindMissing(new int[] { 1,2,3,4 });
        }
    }
}
