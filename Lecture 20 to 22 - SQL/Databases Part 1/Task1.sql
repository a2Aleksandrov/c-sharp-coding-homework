CREATE DATABASE SQLHomework
use SQLHomework;

CREATE TABLE Product(
maker CHAR(1),
model CHAR(4),
type VARCHAR(7)
);
CREATE TABLE Printer(
code INT,
model CHAR(4),
price DECIMAL(7,2)
);

INSERT INTO Product (maker, model, type)
VALUES ('a', 'fghj','askdj');

INSERT INTO Printer (code, model, price)
VALUES ('1525', 'Neon','250.50');

ALTER TABLE Printer ADD CONSTRAINT Type_Constraint CHECK (type	IN ( 'Lazer','Matrix','Jet'));

ALTER TABLE Printer
ADD color CHAR(1) Default ('n');

ALTER TABLE Printer ADD CONSTRAINT Value_Constraint CHECK (color IN ('y','n'));

ALTER TABLE Printer DROP COLUMN price;

DROP TABLE Printer;

Drop Table Product;