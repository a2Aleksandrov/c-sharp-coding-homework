--Databases 3
-- Group by Having

use ships;
--Task1
--1.1
select COUNT(CLASSES.CLASS) as NO_Classes from CLASSES
where CLASSES.TYPE = 'bb';

--1.2
select CLASSES.CLASS, CLASSES.NUMGUNS as avgGuns from CLASSES where
CLASSES.TYPE = 'bb'

--1.3
select AVG(CLASSES.NUMGUNS) as avgGuns from CLASSES
where CLASSES.TYPE = 'bb';

--1.4
select CLASSES.CLASS, MIN(SHIPS.LAUNCHED) as FirstYear, MAX(ships.LAUNCHED) as LastYear from
CLASSES join SHIPS on CLASSES.CLASS = SHIPS.CLASS group by CLASSES.CLASS;

--1.5
select distinct SHIPS.CLASS, COUNT(SHIPS.CLASS) as No_Sunk from SHIPS join OUTCOMES on
SHIPS.NAME = OUTCOMES.SHIP where
OUTCOMES.RESULT = 'sunk' group by SHIPS.CLASS;

--1.6 (doesnt give any result)
select CLASSES.CLASS, COUNT(CLASSES.CLASS) as No_Sunk 
from SHIPS join OUTCOMES
on SHIPS.NAME = OUTCOMES.SHIP  join CLASSES
on SHIPS.CLASS = CLASSES.CLASS
where OUTCOMES.RESULT = 'sunk'
group by CLASSES.CLASS
having COUNT(CLASSES.CLASS) > 2;

--1.7
select CLASSES.COUNTRY, ROUND(AVG(CLASSES.BORE), 2) as avg_bore
from CLASSES join SHIPS
on CLASSES.CLASS = SHIPS.CLASS
group by CLASSES.COUNTRY;

--Task2

--2.1
create nonclustered index SHIPS_NONCL_INDEX
on SHIPS(CLASS);

--2.2

drop index SHIPS.SHIPS_NONCL_INDEX

