--Task 3

--use FLIGHTS;

--3.1
--alter table FLIGHTS
--add num_pass int default 0;

--3.2
--alter table AGENCIES
--add num_book int default 0;

--3.3
--create trigger reservation_trigger
--on BOOKINGS
--after insert
--as
--	update FLIGHTS
--	set num_pass += 1
--	from INSERTED
--	where FLIGHTS.FNUMBER = INSERTED.FLIGHT_NUMBER
--	update AGENCIES
--	set num_book +=1
--	from INSERTED
--	where AGENCIES.NAME = INSERTED.AGENCY

--3.4
--create trigger canceled_reservation_trigger
--on BOOKINGS
--after delete
--as
--	update FLIGHTS
--	set num_pass -= 1
--	from DELETED
--	where FLIGHTS.FNUMBER = DELETED.FLIGHT_NUMBER
--	update AGENCIES
--	set num_book -= 1
--	from DELETED
--	where AGENCIES.NAME = DELETED.AGENCY

--3.5
--create trigger updated_reservation_status_trigger
--on BOOKINGS
--after update
--as
--	if(update (STATUS))
--	BEGIN
	--my logic here is that
	--I have to check if status is changed from 1 to 0 or 0 to 1
	--and add or substract 1 from num_pass but cant get how to do it