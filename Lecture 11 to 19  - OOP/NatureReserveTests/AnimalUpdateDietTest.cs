﻿using NatureReserveSimulation.Food;

namespace NatureReserveTests
{
    [TestClass]
    public class AnimalUpdateDietTest
    {
        [TestMethod]
        public void Diet_Change_When_On_Right_Lifespan()
        {
            var animal = new TestAnimal(Foods.Bear, "testAnimal", 3, new List<Foods>() { Foods.Rabbit });
            var foodInDiet = new TestAnimal(Foods.Rabbit, "Rabbit", 5, new List<Foods>() { Foods.Cabbage });

            animal.Feed(foodInDiet);
            animal.Feed(foodInDiet);
            animal.Feed(foodInDiet);
            animal.Feed(foodInDiet);

            Assert.IsTrue(animal.Diet.Contains(Foods.Berry));
        }
        [TestMethod]
        public void Diet_NotChange_When_Animal_Is_Dead()
        {
            var animal = new TestAnimal(Foods.Bear, "testAnimal", 3, new List<Foods>() { Foods.Rabbit });
            var foodInDiet = new TestAnimal(Foods.Rabbit, "Rabbit", 5, new List<Foods>() { Foods.Cabbage });

            animal.Feed(foodInDiet);
            animal.Feed(foodInDiet);
            animal.Feed(foodInDiet);

            Assert.IsFalse(animal.Diet.Contains(Foods.Berry));
        }
    }
}
