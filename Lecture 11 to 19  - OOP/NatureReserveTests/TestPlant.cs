﻿using NatureReserveSimulation.Food;
using NatureReserveSimulation.LivingBeings;

namespace NatureReserveTests
{
    public class TestPlant : Plant
    {
        public int MaxNutrients { get; }
        public int CurrentNutrients { get; }
        public TestPlant(Foods type, int maxNutrients) : base(type, maxNutrients)
        {

            CurrentNutrients = MaxNutrients = maxNutrients;
        }
    }
}
