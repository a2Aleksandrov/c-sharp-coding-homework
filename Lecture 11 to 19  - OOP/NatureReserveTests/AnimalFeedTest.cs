using NatureReserveSimulation.Food;

namespace NatureReserveTests
{
    [TestClass]
    public class AnimalFeedTest
    {

        [TestMethod]
        public void Animal_FeedOnce_FoodInDiet_MaxEnergy()
        {
            var animal = new TestAnimal(Foods.Bear, "testAnimal", 10, new List<Foods>() { Foods.Rabbit });
            var foodInDiet = new TestAnimal(Foods.Rabbit, "Rabbit", 5, new List<Foods>() { Foods.Cabbage });

            int actualEnergy = animal.Feed(foodInDiet);
            int expectedEnergy = 10;

            Assert.AreEqual(expectedEnergy, actualEnergy);
        }
        [TestMethod]
        public void Animal_Feed_BelowMaxEnergy()
        {
            var animal = new TestAnimal(Foods.Bear, "testAnimal", 10, new List<Foods>() { Foods.Rabbit });
            var foodInDiet = new TestAnimal(Foods.Rabbit, "Rabbit", 1, new List<Foods>() { Foods.Cabbage });
            var foodNotInDiet = new TestAnimal(Foods.Squirrel, "Squirrel", 5, new List<Foods>() { Foods.Nuts });

            animal.Feed(foodNotInDiet);
            animal.Feed(foodNotInDiet);
            int actualEnergy = animal.Feed(foodInDiet);

            int expectedEnergy = 9;

            Assert.AreEqual(expectedEnergy, actualEnergy);
        }
    }
}