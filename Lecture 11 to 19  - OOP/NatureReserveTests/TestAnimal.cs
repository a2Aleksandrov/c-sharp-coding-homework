﻿using NatureReserveSimulation.Food;
using NatureReserveSimulation.LivingBeings;

namespace NatureReserveTests
{
    public class TestAnimal : Animal
    {
        public int MaxEnergy { get; }
        public int CurrentEnergy { get; }
        public List<Foods> Diet { get; set; }
        protected override List<Foods> UpdateDiet()
        {
            if (LifeSpan > 3)
            {
                Diet.Add(Foods.Berry);
            }

            return Diet;
        }

        public TestAnimal(Foods type, string name, int maxEnergy, List<Foods> diet) : base(type, name, maxEnergy, diet)
        {
            CurrentEnergy = MaxEnergy = maxEnergy;
            Diet = diet;
        }
    }
}
