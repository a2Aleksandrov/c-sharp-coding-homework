﻿using NatureReserveSimulation.Food;

namespace NatureReserveTests
{
    [TestClass]
    public class PlantRegenerateTest
    {
        [TestMethod]
        public void Plant_InDiet_Regenerate_After_Each_Cycle()
        {
            var animal = new TestAnimal(Foods.Rabbit, "Rabbit", 4, new List<Foods>() { Foods.Aloe });
            var plantNotInDiet = new TestPlant(Foods.Clementine, 2);
            var plantInDiet = new TestPlant(Foods.Aloe, 2);

            animal.Feed(plantNotInDiet);
            animal.Feed(plantNotInDiet);
            animal.Feed(plantInDiet);
            int actualNutrients = plantInDiet.Grow();
            int expectedNutrients = 1;

            Assert.AreEqual(expectedNutrients, actualNutrients);
        }
        [TestMethod]
        public void Plant_MaxNutrients_Does_Not_Regenerate_After_Each_Cycle()
        {
            var plant = new TestPlant(Foods.Clementine, 2);

            int actualNutrients = plant.Grow();
            int expectedNutrients = 2;

            Assert.AreEqual(expectedNutrients, actualNutrients);
        }
    }
}
