﻿namespace NatureReserveSimulation;
public class Program
{
    private static void Main()
    {
        StartApp();
    }
    public static void StartApp()
    {
        var simulation = new Simulation();

        simulation.Start();
    }
}