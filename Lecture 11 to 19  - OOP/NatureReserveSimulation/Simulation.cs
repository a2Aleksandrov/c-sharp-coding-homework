﻿namespace NatureReserveSimulation
{
    public class Simulation
    {
        public void Start()
        {
            Map.Map.LoadBiomes();

            for (int i = 0; i < Map.Map.Field.GetLength(0); i++)
            {
                for (int j = 0; j < Map.Map.Field.GetLength(1); j++)
                {
                    Map.Map.Field[i, j].MakeTurn();
                }
            }
        }
    }
}
