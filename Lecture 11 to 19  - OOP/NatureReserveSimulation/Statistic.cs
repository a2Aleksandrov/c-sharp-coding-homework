﻿namespace NatureReserveSimulation
{
    public class Statistic
    {
        public string Show(Dictionary<string, List<int>> deadAnimals)
        {
            string statistic = "";
            foreach (var animal in deadAnimals)
            {
                statistic += $"{animal.Key} -> " +
                    $"min lifespan: {animal.Value.Min()} " +
                    $"average lifespan: {animal.Value.Average()} " +
                    $"max lifespan: {animal.Value.Max()}\n";
            }
            Console.WriteLine(statistic);
            return statistic;
        }
    }
}
