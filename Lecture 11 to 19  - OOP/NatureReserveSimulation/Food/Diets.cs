﻿namespace NatureReserveSimulation.Food
{
    public static class Diets
    {
        public static Dictionary<string, List<Foods>> InitialDiet { get; } = new()
        {
            { "bear", new List<Foods>() { Foods.Berry } },
            { "wolf", new List<Foods>() { Foods.Berry } },
            { "wildcat", new List<Foods>() { Foods.Squirrel, Foods.Berry } },
            { "fox", new List<Foods>() { Foods.Berry, Foods.Snake } },
            { "monkey", new List<Foods>() { Foods.Grass } },
            { "beaver", new List<Foods>() { Foods.Bushes } },
            { "snake", new List<Foods>() { Foods.Frog } },
            { "frog", new List<Foods>() { Foods.Azalea, Foods.Blossom } },
            { "mouse", new List<Foods>() { Foods.Azalea } },
            { "deer", new List<Foods>() { Foods.Bushes, Foods.Grass } },
            { "elk", new List<Foods>() { Foods.Bushes, Foods.Berry } },
            { "moose", new List<Foods>() { Foods.Bushes, Foods.Berry } },
            { "zebra", new List<Foods>() { Foods.Bushes, Foods.Berry } },
            { "camel", new List<Foods>() { Foods.DesertFruit } },
            { "cow", new List<Foods>() { Foods.Grass } },
            { "rabbit", new List<Foods>() { Foods.Grass, Foods.Berry, Foods.Bushes } },
            { "squirrel", new List<Foods>() { Foods.Clementine, Foods.Berry, Foods.Bushes } },
        };

        public static List<Foods> Set(string animalType)
        {
            if (InitialDiet.ContainsKey(animalType))
            {
                return InitialDiet[animalType];
            }
            throw new InvalidOperationException("Animal diet does not exist !");
        }
    }
}
