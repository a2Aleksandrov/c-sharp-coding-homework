﻿using NatureReserveSimulation.Map.Biomes;

namespace NatureReserveSimulation.Map
{
    public class CreateBiomFactory
    {
        public enum Biomes
        {
            Desert,
            Mountain,
            Forest,
            Jungle,
            Plain,
            Savanna
        }
        public static Biom CreateRandomBiom(int i, int j)
        {
            Random random = new Random();
            Array values = Enum.GetValues(typeof(Biomes));
            Biomes randomBiom = (Biomes)values.GetValue(random.Next(values.Length))!;
            switch (randomBiom)
            {
                case Biomes.Desert: return new Desert(i, j);
                case Biomes.Mountain: return new Mountain(i, j);
                case Biomes.Forest: return new Forest(i, j);
                case Biomes.Plain: return new Plain(i, j);
                case Biomes.Jungle: return new Jungle(i, j);
                case Biomes.Savanna: return new Savanna(i, j);
                default: return new Savanna(i, j);
            }
        }
    }
}
