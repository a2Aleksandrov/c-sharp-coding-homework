﻿using NatureReserveSimulation.LivingBeings;

namespace NatureReserveSimulation.Map
{
    public abstract class Biom
    {
        private HashSet<Animal> Animals { get; }
        private HashSet<Plant> Plants { get; }
        private int MaxCapacity { get; }
        private int MapCoordinateX { get; }
        private int MapCoordinateY { get; }

        protected Biom(HashSet<Animal> animals, HashSet<Plant> plants, int maxCapacity, int mapCoordinateX, int mapCoordinateY)
        {
            Animals = animals;
            Plants = plants;
            MaxCapacity = maxCapacity;
            MapCoordinateX = mapCoordinateX;
            MapCoordinateY = mapCoordinateY;
        }
        public void MakeTurn()
        {
            var livingBeingsAsFood = GetInhabitants();

            var deadAnimals = new Dictionary<string, List<int>>();
            while (Animals.Count > 0)
            {
                foreach (Animal animal in Animals)
                {
                    animal.OnEveryTurn(this, livingBeingsAsFood);

                    if (!animal.IsAlive)
                    {
                        if (deadAnimals.ContainsKey(animal.ShowType().ToString()))
                        {
                            deadAnimals[animal.ShowType().ToString()].Add(animal.LifeSpan);
                        }
                        else
                        {
                            deadAnimals[animal.ShowType().ToString()] = new List<int>() { animal.LifeSpan };
                        }
                    }
                }
                Animals.RemoveWhere(animalX => animalX.IsAlive == false);

                foreach (Plant plant in Plants)
                {
                    plant.Grow();
                }
            }
            var statistic = new Statistic();
            statistic.Show(deadAnimals);
        }
        public List<Biom> GetNeighbors()
        {
            //Map map = MapFactory.GetMap();

            return Map.GetNeighborBiomes(MapCoordinateX, MapCoordinateY);
        }

        public HashSet<LivingBeing> GetInhabitants()
        {
            var livingBeingsAsFood = new HashSet<LivingBeing>();
            foreach (Animal animal in Animals)
            {
                livingBeingsAsFood.Add(animal);
            }
            foreach (Plant plant in Plants)
            {
                livingBeingsAsFood.Add(plant);
            }
            return livingBeingsAsFood;
        }

        public HashSet<Animal> GetAnimals()
        {
            return Animals;
        }

        public int GetCapacity()
        {
            return MaxCapacity;
        }
    }
}
