﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes;

internal class Desert : Biom
{
    public Desert(int mapCoordinateX, int mapCoordinateY) :
        base(
            new HashSet<Animal>()
    {
        { new Frog() },
        { new Frog() },
        { new Frog() },
        { new Snake() },
        { new Snake() },
        { new Camel() },
        { new Camel() }
    },
            new HashSet<Plant>()
    {
        { new Blossom() },
        { new Clementine() },
        { new DesertFruit() },
        { new DesertFruit() },
        { new Aloe() },
        { new Aloe() }
    },
        15,
        mapCoordinateX,
        mapCoordinateY
    )
    { }
}