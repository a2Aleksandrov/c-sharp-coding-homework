﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes;

internal class Mountain : Biom
{
    public Mountain(int mapCoordinateX, int mapCoordinateY) :
        base(
        new HashSet<Animal>()
    {
        { new Bear() },
        { new Snake() },
        { new Wolf() },
        { new Moose() },
        { new Elk() },
        { new Fox() },
        { new Rabbit() }
    },
        new HashSet<Plant>()
    {
        { new Clementine() },
        { new Berry() },
        { new Blossom() },
        { new Azalea() },
        { new Grass() },
        { new Bushes() },
    },
        21,
        mapCoordinateX,
        mapCoordinateY
        )
    { }
}