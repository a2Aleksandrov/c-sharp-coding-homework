﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes
{
    internal class Plain : Biom
    {
        public Plain(int mapCoordinateX, int mapCoordinateY) :
            base(
                new HashSet<Animal>()
        {
            { new Wolf() },
            { new Cow() },
            { new Rabbit() },
            { new Rabbit() },
            { new Snake() },
            { new Mouse() },
            { new Mouse() },
            { new Fox() },
            { new Deer() }
        }, 
            new HashSet<Plant>()
        {
            { new Azalea() },
            { new Bushes() },
            { new Clementine() },
            { new Blossom() },
            { new Berry() },
            { new Grass() },
            { new Cabbage() },
            { new Carrots() },
            { new Bushes() },
        },
            22,
            mapCoordinateX,
            mapCoordinateY
            )
        { }
    }
}
