﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes;
internal class Savanna : Biom
{
    public Savanna(int mapCoordinateX, int mapCoordinateY) :
        base(
        new HashSet<Animal>()
    {
        { new Snake() },
        { new Snake() },
        { new Moose() },
        { new Moose() },
        { new Elk() },
        { new Zebra() },
        { new Zebra() },
        { new Zebra() },
        { new Wildcat() },
        { new Wildcat() },
        { new Wildcat() }
    },
        new HashSet<Plant>()
    {
        { new Berry() },
        { new Blossom() },
        { new Clementine() },
        { new Grass() },
        { new Bushes() },
        { new Aloe() },
    },
        23,
        mapCoordinateX,
        mapCoordinateY
        )
    { }
}