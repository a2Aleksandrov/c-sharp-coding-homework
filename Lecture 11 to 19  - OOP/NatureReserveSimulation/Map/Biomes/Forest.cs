﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes;

internal class Forest : Biom
{
    public Forest(int mapCoordinateX, int mapCoordinateY) :
        base(
        new HashSet<Animal>()
    {
        { new Bear() },
        { new Bear() },
        { new Snake() },
        { new Snake() },
        { new Wolf() },
        { new Wolf() },
        { new Deer() },
        { new Deer() },
        { new Deer() },
        { new Elk() },
        { new Moose() },
        { new Rabbit() },
        { new Squirrel() },
        { new Squirrel() },
        { new Beaver() },
        { new Fox() }
    },
        new HashSet<Plant>()
    {
        { new Azalea() },
        { new Grass() },
        { new Bushes() },
        { new Grass() },
        { new Berry() },
        { new Nuts() }
        },
        27,
         mapCoordinateX,
         mapCoordinateY
        )
    { }
}