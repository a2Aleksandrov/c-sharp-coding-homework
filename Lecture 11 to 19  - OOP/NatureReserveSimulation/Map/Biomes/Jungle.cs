﻿using NatureReserveSimulation.LivingBeings;
using NatureReserveSimulation.LivingBeings.Animals;
using NatureReserveSimulation.LivingBeings.Plants;

namespace NatureReserveSimulation.Map.Biomes;

internal class Jungle : Biom
{
    public Jungle(int mapCoordinateX, int mapCoordinateY) :
        base(
        new HashSet<Animal>()
    {
        { new Wildcat() },
        { new Wildcat() },
        { new Snake() },
        { new Snake() },
        { new Snake() },
        { new Monkey() },
        { new Monkey() },
        { new Frog() },
        { new Mouse() },
        { new Elk() },
        { new Elk() }
    },
        new HashSet<Plant>()
    {
        { new Clementine()},
        { new Blossom()},
        { new Azalea()},
        { new Bananas()},
        { new Bushes()}
    },
        20,
         mapCoordinateX,
         mapCoordinateY
        )
    { }
}
