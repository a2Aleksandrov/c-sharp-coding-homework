﻿namespace NatureReserveSimulation.Map
{
    public static class Map
    {
        internal static Biom[,] Field = new Biom[2, 3];
        
        public static void LoadBiomes()
        {
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                for (int j = 0; j < Field.GetLength(1); j++)
                {
                    Field[i, j] = CreateBiomFactory.CreateRandomBiom(i, j);
                }
            }
        }
        public static List<Biom> GetNeighborBiomes(int biomCoordinateX, int biomCoordinateY)
        {
            
            var neighborsLocation = new Dictionary<int, int>();

            var xLength = Field.GetLength(1);
            if (biomCoordinateX + 1 < xLength)
            {
                neighborsLocation.Add(biomCoordinateX + 1, biomCoordinateY);
            }
            if (biomCoordinateX - 1 >= 0)
            {
                neighborsLocation.Add(biomCoordinateX - 1, biomCoordinateY);
            }

            var yLength = Field.GetLength(0);
            if (biomCoordinateY + 1 < yLength)
            {
                neighborsLocation.Add(biomCoordinateX, biomCoordinateY + 1);
            }
            if (biomCoordinateY - 1 >= 0)
            {
                neighborsLocation.Add(biomCoordinateX, biomCoordinateY - 1);
            }

            var neighborBiomes = new List<Biom>();
            foreach (var location in neighborsLocation)
            {
                neighborBiomes.Add(Field[location.Key, location.Value]);
            }

            return neighborBiomes;
        }
    }
}