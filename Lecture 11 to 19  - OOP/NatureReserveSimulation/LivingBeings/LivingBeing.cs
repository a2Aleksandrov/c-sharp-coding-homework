﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings
{
    public abstract class LivingBeing
    {
        private Foods Type { get; }
        private int MaxEnergy { get; }
        private int CurrentEnergy { get; set; }
        protected internal int LifeSpan { get; protected set; }

        protected LivingBeing(Foods type, int maxEnergy)
        {
            Type = type;
            CurrentEnergy = MaxEnergy = maxEnergy;
            LifeSpan = 0;
        }

        protected internal int Consume(LivingBeing being)
        {
            int neededEnergy = MaxEnergy - CurrentEnergy;
            if (neededEnergy == 0)
            {
                return CurrentEnergy;
            }
            if (neededEnergy < being.CurrentEnergy)
            {
                CurrentEnergy += neededEnergy;
                being.CurrentEnergy -= neededEnergy;
            }
            else
            {
                CurrentEnergy += being.CurrentEnergy;
                being.CurrentEnergy = 0;
            }
            return CurrentEnergy;
        }

        protected internal int GetEnergy()
        {
            return CurrentEnergy;
        }

        protected internal int GetMaxEnergy()
        {
            return MaxEnergy;
        }

        protected internal int ReduceEnergy()
        {
            CurrentEnergy--;

            return CurrentEnergy;
        }

        protected internal int Regenerate()
        {
            if (CurrentEnergy < MaxEnergy)
            {
                CurrentEnergy++;
            }

            return CurrentEnergy;
        }

        public Foods ShowType()
        {
            return Type;
        }
        protected internal int FinishCycle()
        {
            LifeSpan++;
            return LifeSpan;
        }
    }
}
