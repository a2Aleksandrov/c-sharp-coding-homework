﻿using NatureReserveSimulation.Food;
using NatureReserveSimulation.Map;

namespace NatureReserveSimulation.LivingBeings
{
    public abstract class Animal : LivingBeing
    {
        private string Name { get; }
        protected List<Foods> Diet { get; set; }
        private int starvationLevel { get; set; }
        public bool IsAlive { get; protected set; } = true;

        protected Animal(Foods type, string name, int maxEnergy, List<Foods> initialDiet) : base(type, maxEnergy)
        {
            Name = name;
            Diet = initialDiet;
            starvationLevel = 0;
        }

        public void OnEveryTurn(Biom biom, HashSet<LivingBeing> food)
        {
            Random randomFoods = new();
            var foodItem = food.ElementAt(randomFoods.Next(food.Count));
            if (IsAlive)
            {
                Feed(foodItem);
            }
            int animalEnergy = GetEnergy();
            if (animalEnergy == 0)
            {
                Died();
            }
            else
            {
                if (starvationLevel >= 3)
                {
                    Migrate(biom);
                }
                FinishCycle();
                UpdateDiet();
            }
        }
        private void Feed(LivingBeing foodItem)
        {
            if (Diet.Contains(foodItem.ShowType()))
            {
                Consume(foodItem);
                starvationLevel = 0;
            }
            else
            {
                Starving();
            }
        }

        protected abstract List<Foods> UpdateDiet();
        private protected void Starving()
        {
            starvationLevel++;
            int currentEnergy = ReduceEnergy();
            int maxEnergy = GetMaxEnergy();
            if (currentEnergy <= maxEnergy / 2)
            {
                Console.WriteLine($"{ShowType()} {Name} is starving and making a hungry cry");
            }
        }
        private void Migrate(Biom biom)
        {
            var neighbors = biom.GetNeighbors();

            foreach (var neighbor in neighbors)
            {
                if (neighbor.GetInhabitants().Count < neighbor.GetCapacity())
                {
                    foreach (var livingBeing in neighbor.GetInhabitants())
                    {
                        if (Diet.Contains(livingBeing.ShowType()))
                        {
                            neighbor.GetAnimals().Add(this);
                            biom.GetAnimals().Remove(this);
                            break;
                        }
                    }
                }
            }
        }
        private protected void Died()
        {
            IsAlive = false;
            Console.WriteLine($"{ShowType()} {Name} has died.");
        }
    }
}
