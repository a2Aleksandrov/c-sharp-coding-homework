﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings
{
    public abstract class Plant : LivingBeing
    {
        protected Plant(Foods type, int maxNutrients) : base(type, maxNutrients) { }

        public int Grow()
        {
            int plantNutrients = Regenerate();
            FinishCycle();

            return plantNutrients;
        }
    }
}
