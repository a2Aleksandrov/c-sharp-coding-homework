﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Squirrel : Animal
    {
        protected internal Squirrel() : base(Foods.Squirrel, AnimalName.GenerateName(), 5, Diets.Set("squirrel")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 6:
                    Diet.Add(Foods.Cabbage); break;
                case 7:
                    Diet.Add(Foods.Nuts); break;
            }
            return Diet;
        }
    }
}
