﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Beaver : Animal
    {
        protected internal Beaver() : base(Foods.Beaver, AnimalName.GenerateName(), 9, Diets.Set("beaver")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 4:
                    Diet.Add(Foods.Nuts); break;
            }
            return Diet;
        }
    }
}
