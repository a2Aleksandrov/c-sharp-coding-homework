﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Fox : Animal
    {
        protected internal Fox() : base(Foods.Fox, AnimalName.GenerateName(), 12, Diets.Set("fox")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Squirrel); break;
                case 6:
                    Diet.Add(Foods.Rabbit); break;
            }
            return Diet;
        }
    }
}
