﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Bear : Animal
    {
        protected internal Bear() : base(Foods.Bear, AnimalName.GenerateName(), 15, Diets.Set("bear")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Rabbit); break;
                case 6:
                    Diet.Add(Foods.Deer); break;
            }
            return Diet;
        }
    }
}
