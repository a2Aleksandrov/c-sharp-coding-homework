﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Wildcat : Animal
    {
        protected internal Wildcat() : base(Foods.Wildcat, AnimalName.GenerateName(), 13, Diets.Set("wildcat")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 3:
                    Diet.Add(Foods.Deer); break;
                case 8:
                    Diet.Add(Foods.Moose); break;
            }
            return Diet;
        }
    }
}
