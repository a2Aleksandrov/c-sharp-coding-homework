﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Zebra : Animal
    {
        protected internal Zebra() : base(Foods.Zebra, AnimalName.GenerateName(), 8, Diets.Set("zebra")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 6:
                    Diet.Add(Foods.Blossom); break;
                case 9:
                    Diet.Add(Foods.DesertFruit); break;
            }
            return Diet;
        }
    }
}
