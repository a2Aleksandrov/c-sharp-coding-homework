﻿using NatureReserveSimulation.Food;
namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Rabbit : Animal
    {
        protected internal Rabbit() : base(Foods.Rabbit, AnimalName.GenerateName(), 7, Diets.Set("rabbit")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 3:
                    Diet.Add(Foods.Cabbage); break;
                case 7:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
