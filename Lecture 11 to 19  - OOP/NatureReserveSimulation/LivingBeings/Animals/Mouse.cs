﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Mouse : Animal
    {
        protected internal Mouse() : base(Foods.Mouse, AnimalName.GenerateName(), 5, Diets.Set("mouse")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 4:
                    Diet.Add(Foods.Berry); break;
            }
            return Diet;
        }
    }
}
