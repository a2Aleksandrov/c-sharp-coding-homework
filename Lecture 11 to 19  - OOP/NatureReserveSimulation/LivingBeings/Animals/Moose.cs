﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Moose : Animal
    {
        protected internal Moose() : base(Foods.Moose, AnimalName.GenerateName(), 11, Diets.Set("moose")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 5:
                    Diet.Add(Foods.Blossom); break;
                case 8:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
