﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Snake : Animal
    {
        protected internal Snake() : base(Foods.Snake, AnimalName.GenerateName(), 9, Diets.Set("snake")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 5:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
