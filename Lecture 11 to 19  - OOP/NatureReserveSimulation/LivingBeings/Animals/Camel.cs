﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Camel : Animal
    {
        protected internal Camel() : base(Foods.Camel, AnimalName.GenerateName(), 30, Diets.Set("camel")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 4:
                    Diet.Add(Foods.Nuts); break;
            }
            return Diet;
        }
    }
}
