﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Deer : Animal
    {
        protected internal Deer() : base(Foods.Deer, AnimalName.GenerateName(), 10, Diets.Set("deer")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 1:
                    Diet.Add(Foods.Berry); break;
                case 5:
                    Diet.Add(Foods.Cabbage); break;
            }
            return Diet;
        }
    }
}
