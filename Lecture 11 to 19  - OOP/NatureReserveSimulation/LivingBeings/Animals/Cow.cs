﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Cow : Animal
    {
        protected internal Cow() : base(Foods.Cow, AnimalName.GenerateName(), 9, Diets.Set("cow")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 5:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
