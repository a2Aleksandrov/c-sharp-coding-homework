﻿namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class AnimalName
    {
        public static string GenerateName()
        {
            var chars = "abcdefghijklmnopqrs";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new string(stringChars);
        }
    }
}
