﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Elk : Animal
    {
        protected internal Elk() : base(Foods.Elk, AnimalName.GenerateName(), 11, Diets.Set("elk")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 4:
                    Diet.Add(Foods.Cabbage); break;
                case 7:
                    Diet.Add(Foods.Clementine); break;
            }
            return Diet;
        }
    }
}
