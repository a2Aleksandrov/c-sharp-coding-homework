﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Monkey : Animal
    {
        protected internal Monkey() : base(Foods.Monkey, AnimalName.GenerateName(), 13, Diets.Set("monkey")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Bananas); break;
                case 6:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
