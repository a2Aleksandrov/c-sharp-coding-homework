﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Wolf : Animal
    {
        protected internal Wolf() : base(Foods.Wolf, AnimalName.GenerateName(), 14, Diets.Set("wolf")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 2:
                    Diet.Add(Foods.Cabbage); break;
                case 5:
                    Diet.Add(Foods.Carrots); break;
            }
            return Diet;
        }
    }
}
