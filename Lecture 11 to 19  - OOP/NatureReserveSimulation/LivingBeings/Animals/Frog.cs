﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Animals
{
    public class Frog : Animal
    {
        protected internal Frog() : base(Foods.Frog, AnimalName.GenerateName(), 4, Diets.Set("frog")) { }
        protected override List<Foods> UpdateDiet()
        {
            switch (LifeSpan)
            {
                case 4:
                    Diet.Add(Foods.Berry); break;
            }
            return Diet;
        }
    }
}

