﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Clementine : Plant
    {
        protected internal Clementine() : base(Foods.Clementine, 2) { }
    }
}
