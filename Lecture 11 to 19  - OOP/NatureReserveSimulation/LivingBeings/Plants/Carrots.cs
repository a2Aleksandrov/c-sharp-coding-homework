﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Carrots : Plant
    {
        protected internal Carrots() : base(Foods.Carrots, 5) { }
    }
}
