﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Bananas : Plant
    {
        protected internal Bananas() : base(Foods.Bananas, 6) { }

    }
}
