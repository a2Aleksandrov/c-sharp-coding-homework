﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class DesertFruit : Plant
    {
        protected internal DesertFruit() : base(Foods.DesertFruit, 5) { }

    }
}
