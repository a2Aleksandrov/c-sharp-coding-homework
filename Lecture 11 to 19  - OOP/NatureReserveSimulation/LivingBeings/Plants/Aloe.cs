﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Aloe : Plant
    {
        protected internal Aloe() : base(Foods.Aloe, 3) { }
    }
}
