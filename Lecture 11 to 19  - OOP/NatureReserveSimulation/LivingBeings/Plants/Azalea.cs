﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Azalea : Plant
    {
        protected internal Azalea() : base(Foods.Azalea, 3) { }
    }
}
