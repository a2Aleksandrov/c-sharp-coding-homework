﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Grass : Plant
    {
        protected internal Grass() : base(Foods.Grass, 4) { }
    }
}
