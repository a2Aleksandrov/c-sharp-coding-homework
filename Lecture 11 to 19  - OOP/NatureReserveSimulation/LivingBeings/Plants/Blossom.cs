﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Blossom : Plant
    {
        protected internal Blossom() : base(Foods.Blossom, 3) { }
    }
}
