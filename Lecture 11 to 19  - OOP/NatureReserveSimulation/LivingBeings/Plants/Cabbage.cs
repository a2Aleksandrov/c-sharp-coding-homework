﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Cabbage : Plant
    {
        protected internal Cabbage() : base(Foods.Cabbage, 3) { }
    }
}
