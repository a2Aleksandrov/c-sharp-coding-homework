﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    internal class Bushes : Plant
    {
        protected internal Bushes() : base(Foods.Bushes, 3) { }
    }
}
