﻿
using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Berry : Plant
    {
        protected internal Berry() : base(Foods.Berry, 4) { }
    }
}
