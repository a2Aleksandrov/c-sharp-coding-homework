﻿using NatureReserveSimulation.Food;

namespace NatureReserveSimulation.LivingBeings.Plants
{
    public class Nuts : Plant
    {
        protected internal Nuts() : base(Foods.Nuts, 5) { }
    }
}
