﻿using uniqueElementsNS;
namespace UniqueElementsTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void worksWithListWithConsecutiveEqualElements()
        {
            //Arrange
            List<int> input = new List<int> { 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5 };
            List<int> expected = new List<int> { 1, 2, 3, 4, 5 };

            UniqueElementsClass result = new UniqueElementsClass();
            //Act
            var actual = result.uniqueElements(input);
            bool areEqual = false;
            for (int i = 0; i < actual.Count; i++)
            {
                if (actual[i] == expected[i])
                {
                    areEqual = true;
                }
            }
            //Assert
            Assert.IsTrue(areEqual);
        }
    }
}