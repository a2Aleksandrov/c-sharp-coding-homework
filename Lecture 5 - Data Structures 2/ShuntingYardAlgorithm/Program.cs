﻿
namespace shuntingYardNS
{
    public class Operator
    {
        public char type;
        public int priority;
        public string associativity;

        public Operator(char type, int priority, string associativity)
        {
            this.type = type;
            this.priority = priority;
            this.associativity = associativity;
        }
    }
    public class ShuntingYardAlgorithm
    {
        public static void Main()
        {
            ShuntingYardAlgorithm algorithm = new ShuntingYardAlgorithm();
            Console.Write("Enter infixed notation: ");
            string input = Console.ReadLine();
            algorithm.solver(input);
        }
        public Queue<char> solver(string input)
        {

            char operatorType = ' ';
            int priority = 0;
            string associativity = "";

            Queue<char> result = new Queue<char>();
            Stack<char> operatorStack = new Stack<char>();

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '+' || input[i] == '-' || input[i] == '*' || input[i] == '/' || input[i] == '^' ||
                    input[i] == '(' || input[i] == ')')
                {
                    operatorType = input[i];
                    if (operatorStack.Count == 0)
                    {
                        operatorStack.Push(input[i]);
                    }
                    else
                    {
                        Operator currentOperator = new Operator(operatorType, priority, associativity);

                        switch (operatorType)
                        {
                            case '+': priority = 1; associativity = "left"; break;
                            case '-': priority = 1; associativity = "left"; break;
                            case '*': priority = 2; associativity = "left"; break;
                            case '/': priority = 2; associativity = "left"; break;
                            case '^': priority = 3; associativity = "right"; break;
                        }
                        if (operatorType == '(')
                        {
                            operatorStack.Push(input[i]);
                            priority = 0;
                        }
                        else if (operatorType == ')')
                        {
                            while (operatorStack.Peek() != '(')
                            {
                                result.Enqueue(operatorStack.Pop());
                            }
                            operatorStack.Pop();
                            priority = 0;
                        }
                        else
                        {
                            if (currentOperator.priority < priority)
                            {
                                operatorStack.Push(input[i]);
                            }
                            else if (currentOperator.priority > priority || currentOperator.priority == priority)
                            {
                                result.Enqueue(operatorStack.Pop());
                                operatorStack.Push(input[i]);
                            }
                        }
                    }
                }
                else
                {
                    result.Enqueue(input[i]);
                }
            }
            while (operatorStack.Count != 0)
            {
                result.Enqueue(operatorStack.Pop());
            }
            string r = "";
            while (result.Count != 0)
            {
                r += result.Dequeue();
            }
            Console.WriteLine(r);
            return result;
        }
    }
}