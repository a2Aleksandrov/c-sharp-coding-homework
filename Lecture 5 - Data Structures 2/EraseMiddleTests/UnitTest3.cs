﻿using middleLinkedListNS;
using NuGet.Frameworks;

namespace EraseMiddleTests
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void throwsExceptionWhenEmptyLinkedList()
        {
            //Arrange
            int[] list = { };
            var input = new LinkedList<int>(list);
            int inputLength = input.Count;
            DeleteMiddleClass linkedList = new DeleteMiddleClass();
            //Act
            linkedList.eraseMiddle(input);
            //Assert
            Assert.ThrowsException<SystemException>(); // this line doesnt work. 
            // Cant find how to correct write a test fo checking if a specific error type is shown.
        }
    }
}