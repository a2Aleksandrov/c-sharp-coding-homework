﻿using middleLinkedListNS;
namespace EraseMiddleTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void ReturnsEmptyLinkdeListWhenOnlyOneNode()
        {
            //Arrange
            int[] list = { 1 };
            var input = new LinkedList<int>(list);
            int inputLength = input.Count;
            DeleteMiddleClass linkedList = new DeleteMiddleClass();
            //Act
            linkedList.eraseMiddle(input);
            var lengthAfterDeletion = input.Count;
            //Assert
            Assert.AreNotEqual(inputLength, lengthAfterDeletion);
        }
    }
}