using middleLinkedListNS;
namespace EraseMiddleTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void hasCorrectLengthAfterRemovingElement()
        {
            //Arrange
            int[] list = { 1, 2, 3, 4, 5, 6 };
            var input = new LinkedList<int>(list);
            int inputLength = input.Count;
            DeleteMiddleClass linkedList = new DeleteMiddleClass();
            //Act
            linkedList.eraseMiddle(input);
            var lengthAfterDeletion = input.Count;
            //Assert
            Assert.AreNotEqual(inputLength, lengthAfterDeletion);
        }
    }
}