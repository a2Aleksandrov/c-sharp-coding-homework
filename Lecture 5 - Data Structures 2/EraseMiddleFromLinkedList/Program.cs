﻿using System.Security;

namespace middleLinkedListNS
{
    public class DeleteMiddleClass
    {
        public static void Main()
        {
            int[] arr = { };
            var linkedList = new LinkedList<int>(arr);
            //var first = linkedList.AddFirst(1);
            //var last = linkedList.AddLast(5);
            //var second = linkedList.AddAfter(first, 2);
            //linkedList.AddAfter(second, 3);
            //linkedList.AddBefore(last, 4);
            DeleteMiddleClass result = new DeleteMiddleClass();
            result.eraseMiddle(linkedList);
        }
        public void eraseMiddle(LinkedList<int> input)
        {
            int mid = input.Count / 2;
            var middle = input.First;
            
            while (mid > 0)
            {
                middle = middle!.Next;
                mid--;
            }
            if (middle == null)
            {
                throw new Exception("Not valid input");
            }
            input.Remove(middle);
        }
    }
}