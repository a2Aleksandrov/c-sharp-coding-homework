﻿using Task1;
public class BinaryTree
{
    public int findTreeHeight(BinaryTreeNode input)
    {
        int result = 0;
        if (input != null)
        {
            int lHeight = findTreeHeight(input.Left);
            int rHeight = findTreeHeight(input.Right);
            if (lHeight > rHeight)
            {
                result = lHeight + 1;
            }

            result = rHeight + 1;
        }
        return result;
    }
}

