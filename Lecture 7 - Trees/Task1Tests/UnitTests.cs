using Task1;
namespace Task1Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void checkResultWithNullNode()
        {
            var tree = new BinaryTree();
            var preActual = tree.preOrder(null);
            var postActual = tree.postOrder(null);
            var inActual = tree.inOrder(null);
            string result = null;
            if (preActual == postActual && preActual == inActual)
            {
                result = preActual;
            }
            string expected = "";
            Assert.AreEqual(expected, result);
        }
        public void checkResultWithRootNodeOnly()
        {
            var root = new BinaryTreeNode(5, null, null);
            var tree = new BinaryTree();
            string actualPreOrder = tree.preOrder(root);
            string actualPostOrder = tree.preOrder(root);
            string actualInOrder = tree.preOrder(root);
            string result = "";
            if (actualPreOrder == actualPostOrder && actualPreOrder == actualInOrder)
            {
                result = actualPreOrder;
            }
            string expected = "5";
            Assert.AreEqual(expected, result);
        }
    }
}