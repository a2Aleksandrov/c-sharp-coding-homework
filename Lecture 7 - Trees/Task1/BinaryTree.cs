﻿using Task1;
public class BinaryTree
{
    public string preOrder(BinaryTreeNode input)
    {
        string result = "";
        if (input != null)
        {
            Console.WriteLine(input.Value);
            preOrder(input.Left);
            preOrder(input.Right);
        }
        return result;
    }
    public string postOrder(BinaryTreeNode input)
    {
        string result = "";
        if (input != null)
        {
            postOrder(input.Left);
            postOrder(input.Right);
            Console.WriteLine(input.Value);
        }
        return result;
    }
    public string inOrder(BinaryTreeNode input)
    {
        string result = "";
        if (input != null)
        {
            inOrder(input.Left);
            Console.WriteLine(input.Value);
            inOrder(input.Right);
        }
        return result;
    }
}