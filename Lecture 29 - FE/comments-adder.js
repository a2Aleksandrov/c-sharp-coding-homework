const submitBtn = document.querySelector('.form-button');
submitBtn.addEventListener('click', onSubmit);

const commentsBox = document.querySelector('.comments');

function onSubmit(e) {
    e.preventDefault();
    try {
        const name = document.getElementById('name').value;
        if (name == '') {
            throw new Error('Name cannot be empty.');
        } else {
            document.querySelector('.error').textContent = '';
        }
        const age = document.getElementById('age').value;
        const rating = document.getElementById('rating').value;
        const text = document.getElementById('text').value

        commentsBox.appendChild(addComment(name, age, rating, text));
        
        //saving to file
        const link = document.createElement('a');
        const content = { name, age, rating, text };
        const file = new Blob([JSON.stringify(content)], { type: 'text/json' });
        link.href = URL.createObjectURL(file);
        link.download = 'comments.json';
        link.click();
        URL.revokeObjectURL(link.href);
    } catch (err) {
        document.querySelector('.error').textContent = err.message;
    }
}

async function loader() {
    try {
        const response = await fetch('./comments.json');
        const comments = await response.json();
        Array.from(comments).forEach(comment => {
            commentsBox.appendChild(addComment(comment.name, comment.age, comment.rating, comment.text))
        });
    } catch (err) {
        alert(err.message);
    }
}


function addComment(name, age, rating, content) {
    const contentElement = document.createElement('div');
    contentElement.className = 'content';
    contentElement.textContent = content;

    const userElement = document.createElement('div');
    userElement.className = 'user';

    const usernameElement = document.createElement('span');
    userElement.textContent = `username: ${name}`;

    const ageElement = document.createElement('span');
    ageElement.textContent = `age: ${age}`;

    const ratingElement = document.createElement('span');
    ratingElement.textContent = `rating: ${rating}`;

    userElement.appendChild(usernameElement);
    userElement.appendChild(ageElement);
    userElement.appendChild(ratingElement);

    const currentCommentBox = document.createElement('div');
    currentCommentBox.className = 'user-comment';

    currentCommentBox.appendChild(contentElement);
    currentCommentBox.appendChild(userElement);

    return currentCommentBox;
}