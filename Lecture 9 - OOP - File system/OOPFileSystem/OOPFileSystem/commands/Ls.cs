﻿
namespace OOPFileSystem.commands
{
    internal class Ls
    {
        public FileExplorer executeLs(FileExplorer currentExplorer)
        {
            List<string> list = new List<string>();
            if (currentExplorer.currentFolder.folders.Count != 0)
            {
                foreach (string folderName in currentExplorer.currentFolder.folders.Keys)
                {
                    list.Add(folderName);
                }
            }
            if (currentExplorer.currentFolder.files.Count != 0)
            {
                foreach (File file in currentExplorer.currentFolder.files)
                {
                    list.Add(file.name);
                }
            }
            if (list.Count == 0)
            {
                Console.WriteLine("Directory is empty.");
            }
            else
            {
                Console.WriteLine(String.Join("\n", list.ToArray()));
            }

            return currentExplorer;
        }
    }
}
