﻿namespace OOPFileSystem.commands
{
    public class CD
    {
        public FileExplorer executeCD(FileExplorer explorer, string input)
        {
            if (input == ".." && explorer.directoryPath.Count > 1)
            {
                explorer.directoryPath.Pop();
                explorer.currentFolder = explorer.directoryPath.Peek();
            }
            else
            {
                if (explorer.currentFolder.folders.ContainsKey(input))
                {
                    explorer.currentFolder = explorer.currentFolder.folders[input];
                    explorer.directoryPath.Push(explorer.currentFolder);
                }
                else
                {
                    Console.WriteLine("Invalid operation");
                }
            }
            return explorer;
        }

    }
}
