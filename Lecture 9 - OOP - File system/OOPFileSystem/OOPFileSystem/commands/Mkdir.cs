﻿namespace OOPFileSystem.commands
{
    internal class Mkdir
    {
        public FileExplorer executeMkdir(FileExplorer currentExplorer, string input)
        {
            if (input.Contains("/") || input.Contains("\\") || input.Contains("?") || input.Contains("@"))
            {
                Console.WriteLine("Invalid folder name.");
            }
            else
            {
                Folder newFolder = new Folder(input);
                currentExplorer.currentFolder.folders.Add(input, newFolder);
                Console.WriteLine($"Folder {input} created.");
                //currentExplorer.directoryPath.Push(newFolder);
            }
            return currentExplorer;
        }
    }
}
