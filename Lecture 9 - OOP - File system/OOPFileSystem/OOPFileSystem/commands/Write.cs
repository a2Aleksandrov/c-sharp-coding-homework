﻿namespace OOPFileSystem.commands
{
    internal class Write
    {
        public FileExplorer executeWrite(FileExplorer currentExplorer, string fileName, int line, string entry)
        {
            foreach (File file in currentExplorer.currentFolder.files)
            {
                if (file.name == fileName)
                {
                    if (file.content.ContainsKey(line) && file.content[line] == "")
                    {
                        file.content.Remove(line);
                        file.content.Add(line, entry);
                        break;
                    }
                    else if (file.content.ContainsKey(line) && file.content[line] != "")
                    {
                        Console.WriteLine($"Line {line} is already written.");
                        //logic for override
                        return currentExplorer;
                    }

                    for (int i = file.content.Count; i < line; i++)
                    {
                        if (file.content.ContainsKey(line) && file.content[line] != "")
                        {
                            Console.WriteLine($"Line {line} is already written.");
                            //logic for override
                            return currentExplorer;
                        }
                        else
                        {
                            file.content.Add(i, "");
                        }
                    }

                    file.content.Add(line, entry);
                    break;
                }
            }
            return currentExplorer;
        }
    }
}
