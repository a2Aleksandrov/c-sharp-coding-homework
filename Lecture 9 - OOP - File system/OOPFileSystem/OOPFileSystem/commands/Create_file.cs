﻿namespace OOPFileSystem.commands
{
    internal class Create_file
    {
        public FileExplorer executeCreate_file(FileExplorer currentExplorer, string fileName)
        {
            File newFile = new File(fileName);
            currentExplorer.currentFolder.files.Add(newFile);
            Console.WriteLine("New file created.");
            return currentExplorer;
        }
    }
}
