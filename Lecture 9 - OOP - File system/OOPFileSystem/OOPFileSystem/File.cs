﻿namespace OOPFileSystem
{
    public class File
    {
        public string name { get; set; }
        public Dictionary<int, string> content { get; set; }

        public File(string name)
        {
            this.name = name;
            this.content = new Dictionary<int, string>();
        }
    }
}
