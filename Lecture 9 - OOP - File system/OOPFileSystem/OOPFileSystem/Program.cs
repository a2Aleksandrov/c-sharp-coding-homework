﻿using System.Collections;
using System.Runtime.CompilerServices;
using OOPFileSystem.commands;

namespace OOPFileSystem
{
    class StartAPP
    {
        public static void Main()
        {
            FileExplorer explorer = new FileExplorer("Home");
            readCommand(explorer);
        }
        static void readCommand(FileExplorer currentExplorer)
        {
            string path = "";
            List<string> pathList = new List<string>();
            foreach (Folder folder in currentExplorer.directoryPath)
            {
                pathList.Add(folder.name);
            }
            pathList.Reverse();
            for (int i = 0; i < pathList.Count; i++)
            {
                path += "/" + pathList[i];
            }

            Console.Write(path + ">");
            string input = Console.ReadLine();

            string command = "";
            string argument = "";
            int line = 0;
            string content = "";

            if (input.Contains(" "))
            {
                var data = input.Split(" ");
                if (data[0] != "cd" && data[0] != "mkdir" && data[0] != "create_file" && data[0] != "write" &&
                    data[0] != "cat")
                {
                    Console.WriteLine("No such commmand");
                    readCommand(currentExplorer);
                }
                command = data[0];
                argument = data[1];
                content = "";
                if (data.Length > 2)
                {
                    line = int.Parse(data[2]);
                    content = "";
                    for (int i = 3; i < data.Length; i++)
                    {
                        content += " " + data[i];
                    }
                }
            }
            else if (input != "ls")
            {
                Console.WriteLine("No such commmand");
                readCommand(currentExplorer);
            }
            else
            {
                command = input;
            }
            switch (command)
            {
                case "mkdir": Mkdir mkdir = new Mkdir(); currentExplorer = mkdir.executeMkdir(currentExplorer, argument); break;
                case "cd": CD cd = new CD(); currentExplorer = cd.executeCD(currentExplorer, argument); break;
                case "create_file": Create_file create_file = new Create_file(); currentExplorer = create_file.executeCreate_file(currentExplorer, argument); break;
                case "cat": Cat cat = new Cat(); currentExplorer = cat.executeCat(currentExplorer, argument); break;
                case "write": Write write = new Write(); currentExplorer = write.executeWrite(currentExplorer, argument, line, content); break;
                case "ls": Ls ls = new Ls(); currentExplorer = ls.executeLs(currentExplorer); break;
            }
            readCommand(currentExplorer);
        }
    }
}
