using System.Collections;

namespace OOPFileSystem
{
    public class FileExplorer
    {
        public Folder currentFolder { get; set; }
        public Stack<Folder> directoryPath { get; set; }

        public FileExplorer(string name)
        {
            this.currentFolder = new Folder(name);
            this.directoryPath = new Stack<Folder>();
            directoryPath.Push(currentFolder);
        }
    }
}
