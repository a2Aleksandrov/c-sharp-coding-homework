﻿using System.Collections;

namespace OOPFileSystem
{
    public class Folder
    {
        public string name { get; set; }
        public Dictionary<string, Folder> folders { get; set; }
        public HashSet<File> files { get; set; }

        public Folder(string name)
        {
            this.name = name;
            this.files = new HashSet<File>();
            this.folders = new Dictionary<string, Folder>();
        }
    }
}
