﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Web.Entity.Models;

public partial class Assignments
{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(50)]
    [Unicode(false)]
    public string CreatedBy { get; set; }

    [Required]
    [StringLength(50)]
    [Unicode(false)]
    public string AssignedTo { get; set; }

    [InverseProperty("Assignment")]
    public virtual ICollection<ToDoTasks> ToDoTasks { get; set; } = new List<ToDoTasks>();
}