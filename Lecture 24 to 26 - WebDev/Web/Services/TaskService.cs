﻿using Microsoft.EntityFrameworkCore;
using Web.Entity.Models;
using Web.Models;

namespace Web.Services
{
    public class TaskService : IDisposable
    {
        public List<ToDoTasks> GetAllTasks()
        {
            List<ToDoTasks> tasks;

            using (TasksContext context = new())
            {
                tasks = (from task in context.ToDoTasks
                         join assignment in context.Assignments
                             on task.AssignmentId equals assignment.Id
                         select new ToDoTasks
                         {
                             Id = task.Id,
                             Name = task.Name,
                             DateCreated = task.DateCreated,
                             Done = task.Done,
                             Assignment = assignment
                         }).ToList();
            }
            return tasks;
        }

        public void CreateTask(ToDoTasks newTask)
        {
            using TasksContext context = new();
            context.ToDoTasks.Add(newTask);
            context.SaveChanges();
        }

        public ToDoTasks GetDetails(int id)
        {
            ToDoTasks? currentTask;

            using (TasksContext context = new())
            {
                currentTask = (from task in context.ToDoTasks
                               join assignment in context.Assignments
                                   on task.AssignmentId equals assignment.Id
                               select new ToDoTasks
                               {
                                   Id = task.Id,
                                   Name = task.Name,
                                   DateCreated = task.DateCreated,
                                   Done = task.Done,
                                   Assignment = assignment
                               }).FirstOrDefault(t => t.Id == id);
            }
            return currentTask!;
        }

        public ToDoTasks? ShowTask(int id)
        {
            ToDoTasks? currentTask;
            using (TasksContext context = new())
            {
                currentTask = (from task in context.ToDoTasks
                               join assignment in context.Assignments
                                   on task.AssignmentId equals assignment.Id
                               select new ToDoTasks
                               {
                                   Id = task.Id,
                                   Name = task.Name,
                                   DateCreated = task.DateCreated,
                                   Done = task.Done,
                                   Assignment = assignment
                               }).FirstOrDefault(t => t.Id == id);
            }
            return currentTask;
        }

        public void Update(ToDoTasks edited)
        {
            using (TasksContext context = new TasksContext())
            {
                ToDoTasks task = context.ToDoTasks
                    .Include(t => t.Assignment)
                    .FirstOrDefault(t => t.Id == edited.Id)!;
                task.Name = edited.Name;
                task.DateCreated = edited.DateCreated;
                task.Assignment.CreatedBy = edited.Assignment.CreatedBy;
                task.Assignment.AssignedTo = edited.Assignment.AssignedTo;
                task.Done = edited.Done;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (TasksContext context = new TasksContext())
            {
                ToDoTasks task = context.ToDoTasks.FirstOrDefault(t => t.Id == id)!;
                context.Remove(task);
                context.SaveChanges();
            }
        }
        public void Dispose()
        {

        }
    }
}
