﻿namespace Web.Models
{
    public class Assignment
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        public Assignment(int id, string createdBy, string assignedTo)
        {
            Id = id;
            CreatedBy = createdBy;
            AssignedTo = assignedTo;
        }

        public Assignment()
        {
             
        }
    }
}
