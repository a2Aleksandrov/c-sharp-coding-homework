﻿namespace Web.Models
{
    public class ToDoTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool? Done { get; set; }
        public int AssignmentId { get; set; }
        public Assignment Assignment { get; set; }

        public ToDoTask(int id, string name, string description, DateTime createdOn, bool? done, int assignmentId)
        {
            Id = id;
            Name = name;
            CreatedOn = createdOn;
            Done = done;
            AssignmentId = assignmentId;
        }
        
        public ToDoTask()
        {
        }
    }
}
