﻿using Microsoft.AspNetCore.Mvc;
using Web.Entity.Models;
using Web.Services;

namespace Web.Controllers
{
    public class TaskController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            using TaskService taskService = new();

            var tasks = taskService.GetAllTasks();

            return View(tasks);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ToDoTasks task)
        {
            if (task == null)
            {
                return BadRequest();
            }

            ToDoTasks newTask = new ToDoTasks();

            newTask.Name = task.Name;
            newTask.DateCreated = task.DateCreated;
            newTask.Done = task.Done;
            newTask.Assignment = new Assignments()
            {
                AssignedTo = task.Assignment.AssignedTo,
                CreatedBy = task.Assignment.CreatedBy
            };

            using TaskService taskService = new();
            taskService.CreateTask(newTask);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            using TaskService tasksService = new();
            var taskDetails = tasksService.GetDetails(id);

            return View(taskDetails);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            using TaskService taskService = new();
            var currentTask = taskService.ShowTask(id);
            return View(currentTask);
        }

        [HttpPost]
        public IActionResult Edit(ToDoTasks data)
        {
            using TaskService taskService = new();
            taskService.Update(data);
            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            using TaskService taskService = new();
            taskService.Delete(id);

            return RedirectToAction("Index");
        }
    }
}

