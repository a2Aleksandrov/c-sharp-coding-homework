﻿namespace arraysIntersectionNS
{
    public class ArraysIntersection
    {
        public static void Main()
        {
            ArraysIntersection arrays = new ArraysIntersection();
            int[] arr1 = { 1, 2 };
            int[] arr2 = { 3, 4, 5, 6, 7 };
            arrays.findSameElements(arr1, arr2);
        }
        public int[] findSameElements(int[] arr1, int[] arr2)
        {
            int length = Math.Min(arr1.Length, arr2.Length);
            int[] result = new int[length];

            int index = 0;
            for (int x = 0; x < arr1.Length; x++)
            {
                for (int y = 0; y < arr2.Length; y++)
                {
                    if (arr1[x] == arr2[y])
                    {
                        result.SetValue(arr1[x], index);
                        index++;
                    }
                }
            }
            return result;
        }
    }
}