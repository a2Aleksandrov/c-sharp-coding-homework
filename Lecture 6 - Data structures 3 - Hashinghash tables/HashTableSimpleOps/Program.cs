﻿using System.Collections;

namespace hashTableNS
{
    public class HashTablesOps
    {
        public static void Main()
        {
            Hashtable carToOwner = new Hashtable();
            carToOwner.Add("OB5702BM", "Aleksandar Aleksandrov");
            carToOwner.Add("CB1525BT", "Petar Ivanov");
            carToOwner.Add("CC1111CC", "Miroslav Iliev");
            carToOwner.Add("CB1212CP", "Ivan Ivanov");
            carToOwner.Add("C9999999", "Miroslav Iliev");
            carToOwner.Add("PB5568BB", "Georgi Konstantinov");
            carToOwner.Add("CB1234CP", "Stanislav Petrov");
            HashTablesOps simpleOps = new HashTablesOps();
            simpleOps.findCoolest(carToOwner);
            simpleOps.findOwnersWithMoreThanOneCar(carToOwner);
        }
        public string findCoolest(Hashtable input)
        {
            // coolest plate:
            string result = "";
            foreach (DictionaryEntry car in input)
            {
                if (car.Key.ToString().Contains("9999999"))
                {
                    return car.Value.ToString();
                }
            }
            return "No such car plate.";
        }
        public string findOwnersWithMoreThanOneCar(Hashtable input)
        {
            HashSet<string> owners = new HashSet<string>();
            string result = "";
            foreach (DictionaryEntry car in input)
            {
                owners.Add(car.Value.ToString());
            }
            foreach (string name in owners)
            {
                int count = 0;
                foreach (DictionaryEntry car in input)
                {
                    if (name == car.Value)
                    {
                        count++;
                    }
                }
                if (count > 1)
                {
                    result += $"{name}\n";
                }
            }
            return result == "" ? "No owners with more than one car" : result;
        }
    }
}