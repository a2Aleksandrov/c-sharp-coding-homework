﻿using nonRepeatingCharsNS;
namespace NonRepeatingCharsTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void checkResultWhenAllCharsAreRepeated()
        {
            //Arange
            string input = "asdasd";
            NonRepeatingChars word = new NonRepeatingChars();
            string expected = "";
            //Act
            string actual = word.findNonRepeating(input);
            //Assert
            Assert.AreEqual(actual, expected);
        }
    }
}