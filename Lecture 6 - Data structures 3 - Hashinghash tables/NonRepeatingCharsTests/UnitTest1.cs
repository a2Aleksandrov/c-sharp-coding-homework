using nonRepeatingCharsNS;
namespace NonRepeatingCharsTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checkResultWithEmptyString() 
        {
            //Arange
            string input = "";
            NonRepeatingChars word = new NonRepeatingChars();
            string expected = "";
            //Act
            string actual = word.findNonRepeating(input);
            //Assert
            Assert.AreEqual(actual, expected);
        }
    }
}