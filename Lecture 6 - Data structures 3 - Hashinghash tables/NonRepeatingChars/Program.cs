﻿namespace nonRepeatingCharsNS
{
    public class NonRepeatingChars
    {
        public static void Main()
        {
            string input = "asdfasdfg";
            NonRepeatingChars chars = new NonRepeatingChars();
            chars.findNonRepeating(input);
            chars.firstNonRepeating(input);
        }
        public string findNonRepeating(string input)
        {
            string result = "";
            foreach (char c in input)
            {
                if (input.IndexOf(c) == input.LastIndexOf(c))
                {
                    result += $" {c}";
                }
            }
            return result;
        }
        public int firstNonRepeating(string input)
        {
            int result = 0;
            foreach (char c in input)
            {
                if (input.IndexOf(c) == input.LastIndexOf(c))
                {
                    result = 1;
                }
            }
            return result;
        }
    }
}