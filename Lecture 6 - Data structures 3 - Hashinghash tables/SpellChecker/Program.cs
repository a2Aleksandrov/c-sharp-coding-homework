﻿namespace spellCheckerNS
{
    public class SpellChecker
    {
        public static void Main()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("dog", "dok");
            dict.Add("cat", "cad");
            string document = "We have dok and cad";
            SpellChecker checker = new SpellChecker();
            checker.check(dict, document);
        }
        public  string check(Dictionary<string, string> dict, string text)
        {
            string result = "";
            string[] arrayFromText = text.Split(" ");
            foreach (string item in arrayFromText)
            {
                if (dict.ContainsValue(item))
                {
                    result += $"\n{item}";
                }
            }
            return result;
        }
    }
}