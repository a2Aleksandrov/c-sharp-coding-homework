using arraysIntersectionNS;
namespace IntersectionOfTwoArraysTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checksResultWhenNoSameElements()
        {
            //Arrange
            ArraysIntersection arrays = new ArraysIntersection();
            int[] arr1 = { 1, 2 };
            int[] arr2 = { 3, 4, 5, 6, 7 };
            int[] expected = new int[2];
            //Act
            int[] actual = arrays.findSameElements(arr1, arr2);
            //Assert
            Assert.AreEqual(expected[0], actual[0]);
        }
    }
}