using hashTableNS;
using System.Collections;

namespace HashTableSimpleOpsTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void returnsMsgForNoCoolestPlate()
        {
            //Arrange
            Hashtable carToOwner = new Hashtable();
            carToOwner.Add("OB5702BM", "Aleksandar Aleksandrov");
            carToOwner.Add("CB1525BT", "Petar Ivanov");
            carToOwner.Add("CC1111CC", "Miroslav Iliev");
            HashTablesOps simpleOps = new HashTablesOps();
             string expected = "No such car plate.";
            //Act
            var actual = simpleOps.findCoolest(carToOwner);
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}