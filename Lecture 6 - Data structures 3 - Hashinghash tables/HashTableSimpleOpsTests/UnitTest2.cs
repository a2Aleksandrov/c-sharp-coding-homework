﻿using hashTableNS;
using System.Collections;

namespace HashTableSimpleOpsTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void ReturnMsgWhenNoDataInHashtable()
        {
            //Arrange
            Hashtable carToOwner = new Hashtable();
            HashTablesOps simpleOps = new HashTablesOps();
            string expected = "No such car plate.";
            //Act
            var actual = simpleOps.findCoolest(carToOwner);
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}