using spellCheckerNS;
namespace spellCheckerTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checkResultWithEmptyDictionary()
        {
            SpellChecker checker = new SpellChecker();

            //Arrange
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string text = "the quick brown fox jumps over the lazy dog";
            string expected = "";
            //Act
            string actual = checker.check(dict, text);
            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}